﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Inserte la cantidad de sucursales:");
            int Cantidad = Convert.ToInt32(Console.ReadLine());

            DatosSucursal sucursal = new DatosSucursal();

            sucursal.InsertarDatos(Cantidad);
            sucursal.PromVentas(Cantidad);
            sucursal.PromEmpleados(Cantidad);
            sucursal.CalVenta(Cantidad);
            sucursal.VentaMaxMin(Cantidad);
            sucursal.MostrarDatos(Cantidad);
        }
    }
    class DatosSucursal
    {
        DatosSucursal[] sucursales = new DatosSucursal[99];
        string NombreSucursal, NombreMayor, Nombre1, NombreMenor, Nombre2;
        int MontoVenta;
        int CantEmpleados, CantClientes;
        int TotalVentas, PromedioEmpleados, Promedioventas;
        int sumVentas = 0;
        int sumEmpleados = 0;
        int MayorVenta, MenorVenta;
        int Mayor = 0;
        int Menor = 100000;

        public void InsertarDatos(int CantidadSucursales)
        {
            for (int i = 1; i <= CantidadSucursales; i++)
            {
                Console.WriteLine("Ingrese los datos de la sucursal N°{0}", i);
                sucursales[i] = new DatosSucursal();
                Console.WriteLine("Ingrese el nombre de la Sucursal");
                sucursales[i].NombreSucursal = Console.ReadLine();
                Console.WriteLine("Ingrese el monto de venta de esta sucursal");
                sucursales[i].MontoVenta = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Ingrese cuantos empleados tiene esta sucursal");
                sucursales[i].CantEmpleados = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Ingrese cuantos clientes tuvo este mes");
                sucursales[i].CantClientes = Convert.ToInt32(Console.ReadLine());
            }
        }
        public void PromVentas(int CantidadSucursales)
        {
            for (int i = 1; i <= CantidadSucursales; i++)
            {
                sumVentas += sucursales[i].MontoVenta;
            }
            TotalVentas = sumVentas / CantidadSucursales;
        }
        public void PromEmpleados(int CantidadSucursales)
        {
            for (int i = 1; i <= CantidadSucursales; i++)
            {
                sumEmpleados += sucursales[i].CantEmpleados;
            }
            PromedioEmpleados = sumEmpleados / CantidadSucursales;
        }
        public void CalVenta(int CantidadSucursales)
        {
            for (int i = 1; i <= CantidadSucursales; i++)
            {
                sumVentas += sucursales[i].MontoVenta;
                sumEmpleados += sucursales[i].CantEmpleados;
            }
            Promedioventas = sumVentas / sumEmpleados;
        }
        public void VentaMaxMin(int CantidadSucursales)
        {
            for (int i = 1; i <= CantidadSucursales; i++)
            {
                if (sucursales[i].MontoVenta > Mayor)
                {
                    Mayor = sucursales[i].MontoVenta;
                    NombreMayor = sucursales[i].NombreSucursal;
                }
                if (sucursales[i].MontoVenta < Menor)
                {
                    Menor = sucursales[i].MontoVenta;
                    NombreMenor = sucursales[i].NombreSucursal;
                }
                MayorVenta = Mayor;
                Nombre1 = NombreMayor;

                MenorVenta = Menor;
                Nombre2 = NombreMenor;
            }
        }
        public void MostrarDatos(int CantidadSucursales)
        {
            Console.WriteLine("Listado de Sucursales");
            for (int i = 1; i <= CantidadSucursales; i++)
            {
                Console.WriteLine("Sucursal N°{0}:  {1}", i, sucursales[i].NombreSucursal);
            }
            Console.WriteLine("\n");
            Console.WriteLine("El promedio de Ventas es: {0}", TotalVentas);
            Console.WriteLine("El promedio de Empleados es: {0}", PromedioEmpleados);
            Console.WriteLine("El promedio de Ventas por Empleado es: {0}", Promedioventas);
            Console.WriteLine("La venta maxima fue de ${0} de la sucursal {1}", MayorVenta, Nombre1);
            Console.WriteLine("La venta minima fue de ${0} de la sucursal {1}", MenorVenta, Nombre2);
        }
    }
}