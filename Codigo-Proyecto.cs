﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Inserte la cantidad de sucursales:");
            int Cantidad = Convert.ToInt32(Console.ReadLine());

            DatosSucursal sucursal = new DatosSucursal();

            sucursal.InsertarDatos(Cantidad);
        }
    }
    class DatosSucursal
    {
        DatosSucursal[] sucursales = new DatosSucursal[99];
        string NombreSucursal;
        int MontoVenta;
        int CantEmpleados;
        int CantClientes;

        public DatosSucursal()
        {

        }

        public void InsertarDatos(int CantidadSucursales)
        {
            for (int i = 1; i <= CantidadSucursales; i++)
            {
                Console.WriteLine("Ingrese los datos de la sucursal N°{0}", i);
                sucursales[i] = new DatosSucursal();
                Console.WriteLine("Ingrese el nombre de la Sucursal");
                sucursales[i].NombreSucursal = Console.ReadLine();
                Console.WriteLine("Ingrese el monto de venta de esta sucursal");
                sucursales[i].MontoVenta = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Ingrese cuantos empleados tiene esta sucursal");
                sucursales[i].CantEmpleados = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Ingrese cuantos clientes tuvo este mes");
                sucursales[i].CantClientes = Convert.ToInt32(Console.ReadLine());
            }
        }
    }
}
